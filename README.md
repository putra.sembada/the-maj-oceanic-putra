# Indonesia Luxury Cruises | The MAJ Oceanic Official Site

Book cruises and arrange bespoke itineraries to Raja Ampat and Komodo National Park aboard a traditional handcrafted two-masted Phinisi yacht with 6+1 resort-standard suites, outfitted with modern design and technology.